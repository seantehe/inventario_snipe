<?php

return array(

    'does_not_exist' => 'Accesorio no existe.',
    'assoc_users'	 => 'Este accesorio actualmente tiene :count entregados a usuarios. Por favor ingrese los accesorios y vuelva a intentar. ',

    'create' => array(
        'error'   => 'El Accesorio no ha sido creado, por favor intenta de nuevo. ',
        'success' => 'Accesorio creado con exito.'
    ),

    'update' => array(
        'error'   => 'El Accesorio no ha sido actualizado, favor intentar de nuevo.',
        'success' => 'El Accesorio a sido actulizado satisfactoriamente.'
    ),

    'delete' => array(
        'confirm'   => '¿Estás seguro de eliminar esta categoría?',
        'error'   => 'Ha habido un problema eliminando la categoría. Intentalo de nuevo.',
        'success' => 'La categoría fue eliminada exitosamente.'
    ),

     'checkout' => array(
        'error'   		=> 'El accesorio no fue retirado, por favor vuelva a intentarlo',
        'success' 		=> 'Accesorio retirado correctamente.',
        'user_does_not_exist' => 'Este usuario es inválido . Inténtalo de nuevo.'
    ),

    'checkin' => array(
        'error'   		=> 'El accesorio no fue agregado, favor vuelva a intentarlo',
        'success' 		=> 'Accesorio devuelto correctamente.',
        'user_does_not_exist' => 'Este usuario es inválido . Inténtalo de nuevo.'
    )


);
