<?php

return array(
    'assets_rtd'		=> 'Activos RTD',
    'assets_checkedout'		=> 'Activos cedidos',
    'id'      		=> 'ID',
    'city'   		=> 'Ciudad',
    'state'   		=> 'Provincia',
    'country'   	=> 'Pais',
    'create'		=> 'Crear Localización',
    'update'		=> 'Actualizar Localización',
    'name'			=> 'Nombre Localización',
    'address'		=> 'Dirección',
    'zip'			=> 'Códio Postal',
    'locations'		=> 'Localizaciones',
    'parent'		=> 'Padre',
    'currency'  	=> 'Divisa de la Localización',
);
